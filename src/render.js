export function createItem(row) {
  const li = document.createElement("LI");
  li.classList.add("item");
  li.dataset.uid = row.userID;

  const ranking = document.createElement("div");
  ranking.classList.add("item__ranking");
  ranking.textContent = `# ${row.rank}`;

  const avatar = createAvatar(row.picture);

  const name = document.createElement("div");
  name.classList.add("item__name");
  name.textContent = row.displayName;

  const score = document.createElement("div");
  score.classList.add("item__score");
  score.textContent = `${row.score}pt`;

  li.appendChild(ranking);
  li.appendChild(avatar);
  li.appendChild(name);
  li.appendChild(score);

  return li;
}

function createAvatar(picture) {
  const avatar = document.createElement("div");
  avatar.classList.add("item__avatar");

  const img = document.createElement("img");
  img.classList.add("item__avatar__img");
  img.src = picture;

  avatar.appendChild(img);

  return avatar;
}

export function updateItem(node, row) {
  if (node.dataset.uid === row.userID) {
    return;
  }

  node.dataset.uid = row.userID;

  const nameEl = node.querySelector(".item__name");
  nameEl.textContent = row.displayName;

  const imgEl = node.querySelector(".item__avatar__img");
  imgEl.src = row.picture;

  const scoreEl = node.querySelector(".item__score");
  scoreEl.textContent = `${row.score}pt`;
}
