import "./styles.less";
import { createItem, updateItem } from "./render";
import { genNextData, getInitialData } from "./data";

const root = document.querySelector(".item-wrapper");

function renderInitialData() {
  const fragment = document.createDocumentFragment();
  getInitialData()
    .map(createItem)
    .forEach(li => fragment.appendChild(li));
  root.appendChild(fragment);
}

function renderNextData() {
  const nextData = genNextData();
  nextData.forEach((row, i) => updateItem(root.children[i], row));
}

renderInitialData();

setInterval(() => {
  renderNextData();
}, 1000);
