import data from "./data.json";
import { random } from "./utils";

function mapWithRank(row, i) {
  return { ...row, rank: i + 1 };
}

export function genNextData() {
  const index = random(data.length - 1);
  const nextData = [...data];
  nextData[index].score = nextData[index].score + random(10000);

  return nextData.sort((a, b) => b.score - a.score).map(mapWithRank);
}

export function getInitialData() {
  return data.map(mapWithRank);
}
